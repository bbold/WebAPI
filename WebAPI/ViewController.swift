//
//  ViewController.swift
//  WebAPI
//
//  Created by Mo Lotfi on 21/06/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//

import UIKit
import AFNetworking
import LTMorphingLabel


class ViewController: UIViewController,LTMorphingLabelDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet  var myNameLabel: LTMorphingLabel!
    override func viewDidLoad() {
        let manager = AFHTTPRequestOperationManager()
//        var activityIndicator = UIActivityIndicatorView.self
        self.activityIndicator.hidden = false
        self.activityIndicator.startAnimating()

        

        manager.GET( "https:/graph.facebook.com/bobdylan?access_token=CAACEdEose0cBAAKiAEFoUMDzU10EG8FfxaT5lYs4l5bSZClcgTc6KSIwDtO2jRigo53ZCLkKq8n0pMQugqnmcik3nQBFuxTqHZAUpotKo9MEIRBBBKNPAh52Mt3dvDb1SdX6pd2piXCuXUzDRdqzWSbqbuH2D9c2uRM1fUnVtpE4PhYvotpe5mPEQMGTK5PZA9lvz5mQ5uuqc6i3zkZCw",
            parameters: nil,
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                println("Response: " + responseObject.description)
                if let myName = responseObject["name"] as? String {
                    self.myNameLabel.text = myName
//                    self.myNameLabel.morphingEffect = .Evaporate
                }
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidden = true
            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                println("Error: " + error.localizedDescription)
                self.activityIndicator.stopAnimating()
        })
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        test("fb.com", success: { () -> Void in
            println("success")
        }) { (error) -> Void in
            println(error)
        }
    }
    func test (URL:String, success: () -> Void, failure:(error:NSError) -> Void){
        if URL == "google.com" {
            
        success()
        } else {
            let error = NSError(domain: "", code: 5, userInfo: nil)
            failure(error: error)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

